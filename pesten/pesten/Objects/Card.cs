﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pesten
{
    public class Card
    {
        public string Suit { get; set; }

        public string Name { get; set; }

        public int Value { get; set; }

    }
}
