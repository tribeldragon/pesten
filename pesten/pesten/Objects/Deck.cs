﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pesten
{
    public class Deck
    {
        internal List<Card> Cards { get; set; }


        public Deck BuildDeck()
        {
            Deck _deck = new Deck();
            _deck.Cards = new List<Card>();
            List<string> _cards = System.IO.File.ReadLines("../../pestencards.txt").ToList();

            foreach (string line in _cards)
            {
                List<string> lines  = line.Split(' ').ToList<string>();
                Card card = new Card();

                int lineIteration = 0;


                foreach (string _line in lines)
                {
                    switch(lineIteration)
                    {
                        case 0:
                            card.Value = int.Parse(_line);
                            lineIteration++;
                            break;
                        case 1:
                            card.Name = _line;
                            lineIteration++;
                            break;
                        case 2:
                            card.Suit = _line;
                            lineIteration = 0;
                            break;
                    }
                }
                _deck.Cards.Add(card);
            }


            Random rng = new Random();

            _deck.Cards = _deck.Cards.OrderBy(item => rng.Next()).ToList();
            return  _deck;
        }
    }
}
