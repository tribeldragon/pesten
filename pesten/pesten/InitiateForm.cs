﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pesten
{
    public partial class InitiateForm : Form
    {

        #region
        public InitiateForm()
        {
            InitializeComponent();
        }
        #endregion

        public Deck unplayedCards = null;

        private void button1_Click(object sender, EventArgs e)
        {
            Deck unplayedDeck = new Deck();
            unplayedDeck = unplayedDeck.BuildDeck();

            List<Player> players = new List<Player>();

            int numberOfPlayers = (int)numericUpDown1.Value;

            for (int i = 0; i < numberOfPlayers; i++)
            {
                Player player = new Player();
                player.Name = "Player" + i;

                List<Card> hand = new List<Card>();

                //Creates a player with a card count of 7
                for (int b = 0; b <= 6; b++)
                {
                    Card card = unplayedDeck.Cards.First();
                    unplayedDeck.Cards.Remove(unplayedDeck.Cards.First());
                    hand.Add(card);
                }

                players.Add(player);
            }

            this.Hide();

            GameField gameField = new GameField(unplayedDeck, players);
            gameField.Show();
        }


        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
