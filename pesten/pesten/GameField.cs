﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace pesten
{
    public partial class GameField : Form
    {
        //0 is Clockwise and 1 is Counter Clockwise
        public int GameFlow { get; set; }
        public Deck playedCards { get; set; }
        public int Suit { get; set; }

        public GameField(Deck unplayedDeck, List<Player> players)
        {
            InitializeComponent();
            GameFlow = 0;
        }

        #region Game
        public int PlayCard(Card playedCard, Card topDeck, int pickupCards)
        {
            switch(playedCard.Name)
            {
                case "Ace":
                    if (SameSuit(playedCard.Suit, topDeck.Suit) == true)
                    {
                        if(GameFlow == 0)
                        {
                            GameFlow = 1;
                        }
                        else
                        {
                            GameFlow = 0;
                        }
                        playedCards.Cards.Insert(0, playedCard);

                        MessageBox.Show("Next Player");
                    }
                    else
                    {
                        MessageBox.Show("Suits do not match");
                    }
                    break;
                case "Two":
                    if (SameSuit(playedCard.Suit, topDeck.Suit) == true)
                    {
                        pickupCards = pickupCards + 2;
                        playedCards.Cards.Insert(0, playedCard);
                        MessageBox.Show("Next Player");
                    }
                    else
                    {
                        MessageBox.Show("Suits do not match");
                    }
                    break;
                case "Three":
                case "Four":
                case "Five":
                case "Six":
                case "Nine":
                case "Ten":
                case "Queen":
                case "King":
                    if (SameSuit(playedCard.Suit, topDeck.Suit) == true)
                    {
                        playedCards.Cards.Insert(0, playedCard);
                        MessageBox.Show("Next Player");
                    }
                    else
                    {
                        MessageBox.Show("Suits do not match");
                    }
                    break;
                case "Seven":
                    if (SameSuit(playedCard.Suit, topDeck.Suit) == true)
                    {
                        playedCards.Cards.Insert(0, playedCard);
                        //Same player can play again
                    }
                    else
                    {
                        MessageBox.Show("Suits do not match");
                    }
                    break;
                case "Eight":
                    if (SameSuit(playedCard.Suit, topDeck.Suit) == true)
                    {
                        playedCards.Cards.Insert(0, playedCard);
                        //Next player will be skipped
                    }
                    else
                    {
                        MessageBox.Show("Suits do not match");
                    }
                    break;
                case "Jack":
                    if (SameSuit(playedCard.Suit, topDeck.Suit) == true)
                    {
                        playedCards.Cards.Insert(0, playedCard);
                        //User picks Suit
                        MessageBox.Show("Next Player");
                    }
                    else
                    {
                        MessageBox.Show("Suits do not match");
                    }
                    break;
                case "Joker":
                    if (SameSuit(playedCard.Suit, topDeck.Suit) == true)
                    {
                        pickupCards = pickupCards + 4;
                        playedCards.Cards.Insert(0, playedCard);
                        //User picks Suit
                        MessageBox.Show("Next Player");
                    }
                    else
                    {
                        MessageBox.Show("Suits do not match");
                    }
                    break;
            }
            return pickupCards;
        }

        public bool SameSuit(string suite_PlayedCard, string suite_Topdeck)
        {
            if (suite_PlayedCard == suite_Topdeck) return true;
            else return false;
          
        }
        #endregion
    }
}
